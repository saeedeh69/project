<?php
class Database
{
	private $host;
	private $user;
	private $db;
	private $password;
	private $con;

	public function __construct($host = "localhost", $user = "root", $database = "test", $password = "")
	{
		$this->localhost=$host;
		$this->root=$user;
		$this->test=$database;
		$this->pass=$password;
		$this->con = new PDO("mysql:host=$host; dbname=$database", $user, $password);
		$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
    public function select_all($table)
	{
	
		$query = $this->con->prepare("SELECT * FROM $table");
		$query->execute();
		return $query->fetchAll(PDO::FETCH_ASSOC);
		
	}
	 public function select_row($id,$table)
	{
		$query = $this->con->prepare("SELECT * FROM $table WHERE id=:id");
		$query->bindParam(':id',$id);
		$query->execute();
		return $query->fetch(PDO::FETCH_ASSOC);
	}
	public function insert_row($table,$age,$name)
	{
		$query = $this->con->prepare("INSERT INTO $table VALUES ('null',:age,:name)");
		$query->bindParam(':age',$age);
		$query->bindParam(':name',$name);
		$query->execute();
		return $query;
	}
    public function delete($id,$table)
	{
		if($this->select_row($id,$table))
			{       			
				$query = $this->con->prepare("DELETE FROM $table WHERE id=:id");
				$query->bindParam(':id',$id);
				$query->execute();
				return TRUE;
			}
		else{
			echo "not found";
		}
	}   
}
?>